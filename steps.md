# Crash Meetup : full containerized app with docker

## Présentation

Mise en place d'une application complète front + back avec docker: My favorite quotes

## Etape 1 : 

Prérequis: Création d'une collection de documents avec MongoDB


## Etape 2 : NestJS backend

prérequis: installation NestJS CLI

<code>nest new nestjs-docker</code>

- ajout d'une ressource API
- configuration MongoDB
- lecture d'un document MongoDB
- création du fichier Dockerfile
- build de l'image


## Etape 3 : Front React

- nouveau projet React
- appel d'une ressource API
- création d'un fichier Dockerfile


## Etape 4 : Landing page 

Préparation d'une page statique qui permet d'accueillir, et rediriger l'utilisateur 
vers la partie front React.

## Etape 5 : Reverse proxy

Ajout d'un container docker Nginx

- création du Dockerfile
- fichiers de configuration
- copie des ressources statiques (landing page)

## Etape 6 : docker compose (development)

- création du fichier docker compose

## Etape 7 : Kubernetes (production)

- Création du fichier yaml